const response = {
    async sendResponse(res, status, statusCode, message, data = null,) {
        return res.status(statusCode)
            .send({
                data,
                message,
                status
            })
    }
}

module.exports = { response }